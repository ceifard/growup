import firebase from 'firebase'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyBlpIK1BAA4Q57C_ZJ95mCFm6-KVN0pJ8E",
    authDomain: "growup-b93e1.firebaseapp.com",
    databaseURL: "https://growup-b93e1.firebaseio.com",
    projectId: "growup-b93e1",
    storageBucket: "",
    messagingSenderId: "167655807303"
  };

firebase.initializeApp(config)

// utilidades do firebase
const db = firebase.firestore()
const auth = firebase.auth()

// collections do firebase (bancos de dados)
const series = idUsuario => db.collection(`usuarios/${idUsuario}/series`)
const exerciciosPorDia = idUsuario => db.collection(`usuarios/${idUsuario}/exerciciosPorDia`)
const sugestoesExercicios = idUsuario => db.collection(`selects`)


export {
    db,
    auth,
    series,
    exerciciosPorDia,
    sugestoesExercicios
}