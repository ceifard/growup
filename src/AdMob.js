export function initAd(){
    if ( window.plugins && window.plugins.AdMob ) {
    var ad_units = {
        android : {
            banner: 'ca-app-pub-4424316588756817/7175258229',	//PUT ADMOB ADCODE HERE
            interstitial: 'ca-app-pub-4424316588756817/1418468855',	//PUT ADMOB ADCODE HERE
        },
        // ios : {
        //     banner: 'ca-app-pub-3940256099942544/6300978111',	//PUT ADMOB ADCODE HERE
        //     interstitial: 'ca-app-pub-3940256099942544/1033173712',	//PUT ADMOB ADCODE HERE
        // },
    };
    var admobid = ( /(android)/i.test(navigator.userAgent) ) ? ad_units.android : ad_units.ios;
    
    window.plugins.AdMob.setOptions( {
        publisherId: admobid.banner,
        interstitialAdId: admobid.interstitial,
        adSize: window.plugins.AdMob.AD_SIZE.SMART_BANNER,	//use SMART_BANNER, BANNER, LARGE_BANNER, IAB_MRECT, IAB_BANNER, IAB_LEADERBOARD
        bannerAtTop: true, // set to true, to put banner at top
        overlap: false, // banner will overlap webview
        offsetTopBar: true, // set to true to avoid ios7 status bar overlap
        isTesting: false, // receiving test ad
        autoShow: true // auto show interstitial ad when loaded
      });
      
      registerAdEvents();
      window.plugins.AdMob.createInterstitialView();	//get the interstitials ready to be shown
      window.plugins.AdMob.requestInterstitialAd();
        } else {
        alert( 'admob plugin not ready' );
        }
    }
    //functions to allow you to know when ads are shown, etc.
    export function registerAdEvents() {
    // document.addEventListener('onReceiveAd', function(){
    //     alert('ads received')
    // });
    // document.addEventListener('onFailedToReceiveAd', function(data){
    //     alert(JSON.stringify(data))
    // });
    // document.addEventListener('onPresentAd', function(){
    //     alert('onPresentAd')
    // });
    // document.addEventListener('onDismissAd', function(){
    //     alert('onDismissAd')
    //  });
    // document.addEventListener('onLeaveToAd', function(){
    //     alert('onLeaveToAd')
    //  });
    // document.addEventListener('onReceiveInterstitialAd', function(){
    //     alert('onReceiveInterstitialAd')
    //  });
    // document.addEventListener('onPresentInterstitialAd', function(){
    //     alert('onPresentInterstitialAd')
    //  });
    // document.addEventListener('onDismissInterstitialAd', function(){
    //     alert('onDismissInterstitialAd')
    window.plugins.AdMob.createInterstitialView();	//REMOVE THESE 2 LINES IF USING AUTOSHOW
    window.plugins.AdMob.requestInterstitialAd();	//get the next one ready only after the current one is closed
    //});
    }
    
    //display the banner
    export function showBannerFunc(){
    window.plugins.AdMob.createBannerView();
    }
    //display the interstitial
    export function showInterstitialFunc(){
    window.plugins.AdMob.showInterstitialAd();
    }