export let ModelExercicios = {
    computed: {
        nomeExercicio: {
            get() {
                return this.$store.getters['exercicios/nome']
            },
            set(value) {
                this.$store.commit('exercicios/nome', value)
            }
        },
        cargaExercicio: {
            get() {
                return this.$store.getters['exercicios/carga']
            },
            set(value) {
                let valorFormatado = value.replace(' kg', '')
                this.$store.commit('exercicios/carga', valorFormatado)
            }
        },
        qtdSeriesExercicio: {
            get() {
                return this.$store.getters['exercicios/qtdSeries']
            },
            set(value) {
                this.$store.commit('exercicios/qtdSeries', value)
            }
        },                
        qtdRepeticoesExercicio: {
            get() {
                return this.$store.getters['exercicios/qtdRepeticoes']
            },
            set(value) {
                this.$store.commit('exercicios/qtdRepeticoes', value)
            }
        },
        exercicioDoDia: {
            get() {
                return this.$store.getters['exercicios/exercicioDoDia']
            },
            set(value) {
                this.$store.commit('exercicios/exercicioDoDia', value)
            }                
        },            
        exerciciosDoDia: {
            get() {
                return this.$store.getters['exercicios/exerciciosDoDia']
            },
            set(value) {
                this.$store.commit('exercicios/exerciciosDoDia', value)
            }                
        }           
    },
  }