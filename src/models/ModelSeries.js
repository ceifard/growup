export let ModelSeries = {
    computed: {
        nomeSerie: {
            get() {
                return this.$store.getters['series/nomeSerie']
            },
            set(value) {
                this.$store.commit('series/nomeSerie', value)
            }
        },
        descricaoSerie: {
            get() {
                return this.$store.getters['series/descricaoSerie']
            },
            set(value) {
                this.$store.commit('series/descricaoSerie', value)
            }
        }          
    },
  }