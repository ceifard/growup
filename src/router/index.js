import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

const fb = require('../firebaseConfig')

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(x => x.meta.requiresAuth) //checa se, na rota que estamos sendo direcionados, há o meta "requiresAuth" setado nas rotas de routes.js
    const currentUser = fb.auth.currentUser  //checa se tem usuario logado, se tiver retorna o usuario, se nao tiver retorna null
    currentUser ? store.commit('usuarioLogado', true) : store.commit('usuarioLogado', false);

    if (requiresAuth && !currentUser) {  //se a rota que estamos navegando requer autenticação e nao há usuário logado, redirecionamos para a pagina de login
        next('login') 
    } else if (!requiresAuth && currentUser) {  //se a rota que estamos navegando NÃO requer autenticação (cadastro e login) e há usuário logado, redirecionamos para a pagina de series
        next('listagem-series');
    } else { //senão, deixamos a navegação prosseguir
        next()
    }

  }) 

  return Router
}
