

const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { 
        path: '', 
        redirect: '/listagem-series', 
        meta: {
          requiresAuth: true
        },             
      },      
      { 
        path: '/listagem-series', 
        component: () => import('components/pages/series/Listagem.vue'),
        name: 'listagem-series',
        meta: {
          requiresAuth: true
        },        
      },
      { 
        path: '/cadastro-serie', 
        component: () => import('components/pages/series/Cadastro.vue'),
        name: 'cadastro-serie',
        meta: {
          requiresAuth: true
        },        
      },  
      { 
        path: '/listagem-exercicios', 
        component: () => import('components/pages/exercicios/Listagem.vue'),
        name: 'listagem-exercicios',
        meta: {
          requiresAuth: true
        },        
      },
      { 
        path: '/cadastro-exercicio', 
        component: () => import('components/pages/exercicios/Cadastro.vue'),
        name: 'cadastro-exercicio',
        meta: {
          requiresAuth: true
        },        
      },           
      { 
        path: '/login', 
        name: 'login',
        component: () => import('components/pages/autenticacao/Login.vue') 
      },
      { 
        path: '/cadastro', 
        name: 'cadastro',
        component: () => import('components/pages/autenticacao/Cadastro.vue') 
      },                 
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
