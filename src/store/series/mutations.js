export function idSerieSelecionada (state, value) {
    state.idSerieSelecionada = value
}

export function serie (state, value) {
    state.serie = value
}

export function nomeSerie (state, value) {
    state.serie.nomeSerie = value
}

export function descricaoSerie (state, value) {
    state.serie.descricaoSerie = value
}

export function series (state, value) {
    state.series = value
}
