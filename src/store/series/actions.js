const fb = require('../../firebaseConfig.js')

export function cadastraSerie ({getters, commit, rootGetters}, router) {
    commit('carregando', true, { root: true });
    return fb.series(rootGetters['idUsuario']).add(getters.serie).then(res => { 
        fb.exerciciosPorDia(rootGetters['idUsuario']).add({idSerie: res.id, data: new Date(), exerciciosDoDia: rootGetters['exercicios/exerciciosDoDia']}).then( () => {
            commit('mensagem', {tipo: 'positive', texto: 'Série adicionada com sucesso!'}, { root: true });
            router.push('/listagem-series')
            commit('carregando', false, { root: true });            
        } )
    })    
}

export function buscaSerie ({getters, commit}) {
}

export function deletaSerie ({getters, commit}) {
}

export function listaSeries ({getters, commit, rootGetters}) {
    commit('carregando', true, { root: true });
    return fb.series(rootGetters['idUsuario']).get().then(querySnapshot => {
        let series = [];
        querySnapshot.forEach(res => {
            let serie = res.data();
            serie.id = res.id;
            series.push(serie);
        })
        commit('series', series);
        commit('carregando', false, { root: true });
    })
}
