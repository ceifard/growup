export const idSerieSelecionada = (state) => state.idSerieSelecionada
export const serie = (state) => state.serie
export const nomeSerie = (state) => state.serie.nomeSerie
export const descricaoSerie = (state) => state.serie.descricaoSerie
export const exerciciosIniciaisSerie = (state) => state.serie.exerciciosIniciaisSerie
export const series = (state) => state.series
