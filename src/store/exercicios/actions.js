const fb = require('../../firebaseConfig.js')

export function cadastraExerciciosDoDia ({getters, commit, rootGetters}, [router, idSerie, exerciciosDoDia]) {
    commit('carregando', true, { root: true });
    return fb.exerciciosPorDia(rootGetters['idUsuario']).add({'idSerie': idSerie, 'data': new Date(), 'exerciciosDoDia': exerciciosDoDia}).then(() => {
        commit('mensagem', {tipo: 'positive', texto: 'Evolução adicionada com sucesso!'}, { root: true });  
        router.push('/listagem-exercicios')
        commit('carregando', false, { root: true });
    })    
}

export function deletaExerciciosDoDia ({getters, commit}) {
}

export function listaExerciciosPorDia ({getters, commit, rootGetters}) {
    commit('carregando', true, { root: true });
    return fb.exerciciosPorDia(rootGetters['idUsuario']).where("idSerie", "==", rootGetters['series/idSerieSelecionada']).get()
    .then(querySnapshot => {
        let exercicios = [];
        querySnapshot.forEach(res => {
            //console.log(res.data())
            let exercicio = res.data();
            //a ORIGEM vinha como array e o resto como objeto, passei a transformar tudo em array.
            exercicio.exerciciosDoDia = Object.values(exercicio.exerciciosDoDia)
            exercicios.push(exercicio);
        })
        exercicios.sort(function(a,b){
            return new Date(b.data.toDate()) - new Date(a.data.toDate());
        });          
        commit('exerciciosPorDia', exercicios);
        commit('carregando', false, { root: true });
    })
}


export function buscaSugestoes ({getters, commit, rootGetters}) {
    commit('carregando', true, { root: true });
    return fb.sugestoesExercicios(rootGetters['idUsuario']).get()
    .then(querySnapshot => {
        let sugestoes = [];
        querySnapshot.forEach(res => {
            sugestoes = res.data().sugestoesExercicios;
        })      
        commit('sugestoesExercicios', sugestoes);
        commit('carregando', false, { root: true });
    })    
}