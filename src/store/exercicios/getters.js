export const exercicioDoDia = (state) => state.exercicioDoDia
export const nome = (state) => state.exercicioDoDia.nome
export const carga = (state) => state.exercicioDoDia.carga
export const qtdSeries = (state) => state.exercicioDoDia.qtdSeries
export const qtdRepeticoes = (state) => state.exercicioDoDia.qtdRepeticoes
export const exerciciosDoDia = (state) => state.exerciciosDoDia
export const exerciciosPorDia = (state) => state.exerciciosPorDia
export const sugestoesExercicios = (state) => state.sugestoesExercicios
