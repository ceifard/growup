import Vue from 'vue'
import Vuex from 'vuex'

import series from './series'
import exercicios from './exercicios'

Vue.use(Vuex)

const state = {
  carregando: false,
  mensagem: {tipo: '', texto: ''},
  usuarioLogado: false,
  idUsuario: '',
  emailUsuario: '',
}

const getters = {
  carregando: state => state.carregando,
  mensagem: state => state.mensagem,
  usuarioLogado: state => state.usuarioLogado,
  idUsuario: state => state.idUsuario,
  emailUsuario: state => state.emailUsuario
}

const mutations = { 
  carregando(state, obj) {
      state.carregando = obj;
  },
  mensagem(state, obj) {
      state.mensagem = obj;
  },
  usuarioLogado(state, obj) {
      state.usuarioLogado = obj;
  },
  idUsuario(state, obj) {
    state.idUsuario = obj;
},  
  emailUsuario(state, obj) {
    state.emailUsuario = obj;
  }              
}

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state,
    mutations,
    getters,    
    modules: {
      series,
      exercicios
    }
  })

  return Store
}
